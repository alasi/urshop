using System;
using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Orders;
using Urs.Data.Domain.Payments;
using Urs.Data.Domain.Shipping;

namespace Urs.Services.Orders
{
    public partial interface IOrderReportService
    {
        OrderAverageReportLine GetOrderAverageReportLine(List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
            DateTime? startTimeUtc = null, DateTime? endTimeUtc = null, bool ignoreCancelledOrders = false);

        OrderAverageReportLineSummary OrderAverageReport(OrderStatus os);

        IList<BestsellersReportLine> BestSellersReport(int? categoryId = null, int brandId = 0, DateTime? startTime = null,
            DateTime? endTime = null, OrderStatus? os = null, PaymentStatus? ps = null, ShippingStatus? ss = null,
            int recordsToReturn = 5, int orderBy = 1, int groupBy = 1, bool showHidden = false);

        IList<Goods> GetGoodssAlsoPurchasedById(int goodsId,
            int recordsToReturn = 5, bool showHidden = false);

        IPagedList<Goods> GoodssNeverSold(DateTime? startTime,
            DateTime? endTime, int pageIndex, int pageSize, bool showHidden = false);

        decimal ProfitReport(List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
            DateTime? startTimeUtc = null, DateTime? endTimeUtc = null);

    }
}
