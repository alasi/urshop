using System;
using System.Linq;
using Urs.Core;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Configuration;
using Urs.Services.Localization;
using Urs.Services.Security;

namespace Urs.Services.Users
{
    public partial class UserRegistrationService : IUserRegistrationService
    {
        #region Fields
        private readonly IUserService _userService;
        private readonly IEncryptionService _encryptionService;
        private readonly ILocalizationService _localizationService;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly UserSettings _userSettings;

        #endregion

        #region Ctor

        public UserRegistrationService(
            IUserService userService,
            IEncryptionService encryptionService,
            ILocalizationService localizationService,
            RewardPointsSettings rewardPointsSettings, UserSettings userSettings)
        {
            this._userService = userService;
            this._encryptionService = encryptionService;
            this._localizationService = localizationService;
            this._rewardPointsSettings = rewardPointsSettings;
            this._userSettings = userSettings;
        }

        #endregion

        #region Methods

        public virtual bool Validate(string usernameOrEmailOrPhone, string password)
        {
            User user = _userService.GetUsersByUsernameOrPhoneOrEmail(usernameOrEmailOrPhone);

            if (user == null || user.Deleted)
                return false;

            if (!user.IsRegistered())
                return false;

            string pwd = "";
            switch (user.PasswordFormat)
            {
                case PasswordFormat.Encrypted:
                    pwd = _encryptionService.EncryptText(password);
                    break;
                case PasswordFormat.Hashed:
                    pwd = _encryptionService.CreatePasswordHash(password, user.PasswordSalt, _userSettings.HashedPasswordFormat);
                    break;
                default:
                    pwd = password;
                    break;
            }

            bool isValid = pwd == user.Password;

            if (isValid)
            {
                user.LastLoginTime = DateTime.Now;
                _userService.UpdateUser(user);
            }

            return isValid;
        }

        public virtual UserRegistrationResult RegisterUser(UserRegistrationRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            var result = new UserRegistrationResult();
            switch (request.UserRegistrationType)
            {
                case UserRegistrationType.Phone:
                    {
                        if (string.IsNullOrEmpty(request.Phone))
                        {
                            result.AddError(_localizationService.GetResource("Account.Register.Errors.AccountNameIsNotProvided"));
                            return result;
                        }
                        if (_userService.GetUsersByUsernameOrPhoneOrEmail(request.Phone) != null)
                        {
                            result.AddError(_localizationService.GetResource("Account.Register.Errors.AccountNameAlreadyExists"));
                            return result;
                        }
                    }
                    break;
                case UserRegistrationType.AutoToken:
                    {
                        request.Password = CommonHelper.RandomCode(12);
                    }
                    break;
            }
            if (String.IsNullOrWhiteSpace(request.Password))
            {
                result.AddError(_localizationService.GetResource("Account.Register.Errors.PasswordIsNotProvided"));
                return result;
            }
            if (request.User == null)
                request.User = _userService.InsertGuestUser();
            if (!string.IsNullOrEmpty(request.Email))
                request.User.Email = request.Email;
            if (!string.IsNullOrEmpty(request.Phone))
                request.User.PhoneNumber = request.Phone;
            if (!string.IsNullOrEmpty(request.Nickname))
                request.User.Nickname = request.Nickname;
            request.User.PasswordFormat = request.PasswordFormat;
            if (request.UserRegistrationType != UserRegistrationType.AutoToken)
                switch (request.PasswordFormat)
                {
                    case PasswordFormat.Clear:
                        {
                            request.User.Password = request.Password;
                        }
                        break;
                    case PasswordFormat.Encrypted:
                        {
                            request.User.Password = _encryptionService.EncryptText(request.Password);
                        }
                        break;
                    case PasswordFormat.Hashed:
                        {
                            string saltKey = _encryptionService.CreateSaltKey(5);
                            request.User.PasswordSalt = saltKey;
                            request.User.Password = _encryptionService.CreatePasswordHash(request.Password, saltKey, _userSettings.HashedPasswordFormat);
                        }
                        break;
                    default:
                        break;
                }

            request.User.Active = request.IsApproved;

            var guestRole = request.User.UserRoles.FirstOrDefault(cr => cr.SystemName == SystemRoleNames.Guests);
            if (guestRole != null)
                request.User.UserRoleMappings.Remove(request.User.UserRoleMappings.FirstOrDefault(mapping => mapping.UserRoleId == guestRole.Id));
            var registeredRole = _userService.GetUserRoleBySystemName(SystemRoleNames.Registered);
            if (registeredRole == null)
                throw new UrsException("'Registered' role could not be loaded");

            var regRole = request.User.UserRoles.FirstOrDefault(cr => cr.SystemName == SystemRoleNames.Registered);
            if (regRole == null)
                request.User.UserRoleMappings.Add(new UserRoleMapping { UserRole = registeredRole });

            if (_rewardPointsSettings.Enabled &&
                _rewardPointsSettings.PointsForRegistration > 0)
                request.User.AddRewardPointsHistoryEntry(_rewardPointsSettings.PointsForRegistration, "�û�ע��");

            _userService.UpdateUser(request.User);
            return result;
        }

        public virtual PasswordRequestResult CheckPassword(CheckPasswordRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            var result = new PasswordRequestResult();

            if (String.IsNullOrWhiteSpace(request.ConfirmPassword))
            {
                result.AddError(_localizationService.GetResource("Account.ChangePassword.Errors.PasswordIsNotProvided"));
                return result;
            }
            string confirmPassword = string.Empty;
            switch (request.User.PasswordFormat)
            {
                case PasswordFormat.Encrypted:
                    confirmPassword = _encryptionService.EncryptText(request.ConfirmPassword);
                    break;
                case PasswordFormat.Hashed:
                    confirmPassword = _encryptionService.CreatePasswordHash(request.ConfirmPassword, request.User.PasswordSalt, _userSettings.HashedPasswordFormat);
                    break;
                default:
                    confirmPassword = request.ConfirmPassword;
                    break;
            }

            bool passwordIsValid = confirmPassword == request.User.Password;
            if (!passwordIsValid)
                result.AddError(_localizationService.GetResource("Account.ChangePassword.Errors.ConfirmPasswordDoesntMatch"));

            return result;
        }

        public virtual PasswordRequestResult ChangePassword(ChangePasswordRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            var result = new PasswordRequestResult();

            if (String.IsNullOrWhiteSpace(request.NewPassword))
            {
                result.AddError(_localizationService.GetResource("Account.ChangePassword.Errors.PasswordIsNotProvided"));
                return result;
            }

            var user = _userService.GetUserByGuid(request.UserGuid);
            if (user == null)
            {
                result.AddError(_localizationService.GetResource("Account.ChangePassword.Errors.AccountNameNotFound"));
                return result;
            }


            var requestIsValid = false;
            if (request.ValidateRequest)
            {
                string oldPwd = "";
                switch (user.PasswordFormat)
                {
                    case PasswordFormat.Encrypted:
                        oldPwd = _encryptionService.EncryptText(request.OldPassword);
                        break;
                    case PasswordFormat.Hashed:
                        oldPwd = _encryptionService.CreatePasswordHash(request.OldPassword, user.PasswordSalt, _userSettings.HashedPasswordFormat);
                        break;
                    default:
                        oldPwd = request.OldPassword;
                        break;
                }

                bool oldPasswordIsValid = oldPwd == user.Password;
                if (!oldPasswordIsValid)
                    result.AddError(_localizationService.GetResource("Account.ChangePassword.Errors.OldPasswordDoesntMatch"));

                if (oldPasswordIsValid)
                    requestIsValid = true;
            }
            else
                requestIsValid = true;


            if (requestIsValid)
            {
                switch (request.NewPasswordFormat)
                {
                    case PasswordFormat.Clear:
                        {
                            user.Password = request.NewPassword;
                        }
                        break;
                    case PasswordFormat.Encrypted:
                        {
                            user.Password = _encryptionService.EncryptText(request.NewPassword);
                        }
                        break;
                    case PasswordFormat.Hashed:
                        {
                            string saltKey = _encryptionService.CreateSaltKey(5);
                            user.PasswordSalt = saltKey;
                            user.Password = _encryptionService.CreatePasswordHash(request.NewPassword, saltKey, _userSettings.HashedPasswordFormat);
                        }
                        break;
                    default:
                        break;
                }
                user.PasswordFormat = request.NewPasswordFormat;
                _userService.UpdateUser(user);
            }

            return result;
        }

        public virtual void SetPhone(User user, string newPhone)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            newPhone = newPhone.Trim();
            string oldPhone = user.PhoneNumber;

            if (!CommonHelper.IsValidPhone(newPhone))
                throw new UrsException(_localizationService.GetResource("Account.Errors.NewPhoneIsNotValid"));

            var user2 = _userService.GetUserByPhone(newPhone);
            if (user2 != null && user.Id != user2.Id)
                throw new UrsException(_localizationService.GetResource("Account.Errors.PhoneAlreadyExists"));

            user.PhoneNumber = newPhone;
            _userService.UpdateUser(user);
        }

        #endregion
    }
}