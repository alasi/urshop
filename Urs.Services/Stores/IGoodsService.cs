using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Orders;
using System;
using Urs.Data.Domain.Users;

namespace Urs.Services.Stores
{
    public partial interface IGoodsService
    {
        #region Goodss

        void DeleteGoods(Goods goods);

        IList<Goods> GetSpecialGoodss(int takeCount = 0);
        IList<Goods> GetAllGoodssDisplayedOnHomePage();

        Goods GetGoodsById(int goodsId);

        Goods GetGoodsBySku(string sku);

        IList<Goods> GetGoodssByIds(int[] goodsIds, bool showHidden = true);
        IList<Goods> GetFeaturedGoodsByCategoryId(int categoryId, int number);
        void InsertGoods(Goods goods);

        void UpdateGoods(Goods goods);

        int GetCategoryGoodsNumber(IList<int> categoryIds = null);

        IPagedList<Goods> SearchGoodss(
            out IList<int> filterableGoodsParameterOptionIds, bool loadFilterableGoodsParameterOptionIds = false,
            int[] categoryId = null, int brandId = 0, int creatorId = 0, bool? featuredGoodss = null,
            decimal? priceMin = null, decimal? priceMax = null, int goodsTagId = 0,
            string keywords = null, bool searchDescriptions = false, bool searchGoodsTags = false,
            IList<int> filteredSpecs = null, GoodsSortingEnum orderBy = GoodsSortingEnum.Position,
            int pageIndex = 0, int pageSize = int.MaxValue,
            bool showHidden = false, string sku = null);

        void UpdateGoodsReviewTotals(Goods goods);

        IPagedList<Goods> GetLowStockGoodss(int pageIndex = 0, int pageSize = int.MaxValue);

        IPagedList<GoodsSpecCombination> GetLowStockGoodsCombinations(int pageIndex = 0, int pageSize = int.MaxValue);
        void AdjustInventory(Goods goods, bool decrease,
            int quantity, string attributesXml);

        #endregion

        #region Goods pictures

        void DeleteGoodsPicture(GoodsPicture goodsPicture);

        IList<GoodsPicture> GetGoodsPicturesByGoodsId(int goodsId);

        GoodsPicture GetGoodsPictureById(int goodsPictureId);

        void InsertGoodsPicture(GoodsPicture goodsPicture);

        void UpdateGoodsPicture(GoodsPicture goodsPicture);

        #endregion

        #region GoodsReviews

        void DeleteGoodsReview(GoodsReview goodsReview);

        IPagedList<GoodsReview> GetGoodsReviews(int? goodsId, int? orderId, int? userId, bool? isApproved = false,
            DateTime? startTime = null, DateTime? endTime = null, int pageIndex = 0, int pageSize = int.MaxValue);

        GoodsReview GetGoodsReview(int goodsId, int orderId);

        GoodsReview GetGoodsReviewById(int goodsReviewId);

        void InsertGoodsReview(GoodsReview goodsReview);

        void UpdateGoodsReview(GoodsReview goodsReview);

        #endregion

        #region Goods Tag

        IList<Goods> GetGoodsByTag(int goodsTagId, bool random = false, int count = 6);

        #endregion
    }
}
