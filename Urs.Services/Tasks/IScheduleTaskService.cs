using System.Collections.Generic;
using Urs.Data.Domain.Tasks;

namespace Urs.Services.Tasks
{
    public partial interface IScheduleTaskService
    {
        void DeleteTask(ScheduleTask task);

        ScheduleTask GetTaskById(int taskId);

        ScheduleTask GetTaskByType(string type);

        IList<ScheduleTask> GetAllTasks(bool showHidden = false);

        void InsertTask(ScheduleTask task);

        void UpdateTask(ScheduleTask task);
    }
}
