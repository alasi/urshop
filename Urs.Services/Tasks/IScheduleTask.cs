﻿
namespace Urs.Services.Tasks
{
    public partial interface IScheduleTask
    {
        void Execute();
    }
}
