using System.Collections.Generic;
using Urs.Data.Domain.Localization;

namespace Urs.Services.Localization
{
    public partial interface ILocalizationService
    {
        void DeleteLocaleStringResource(LocaleStringResource localeStringResource);

        LocaleStringResource GetLocaleStringResourceById(int localeStringResourceId);

        LocaleStringResource GetLocaleStringResourceByName(string resourceName);

        LocaleStringResource GetLocaleStringResourceByName(string resourceName,  bool logIfNotFound = true);

        IList<LocaleStringResource> GetAllResources();

        void InsertLocaleStringResource(LocaleStringResource localeStringResource);

        void UpdateLocaleStringResource(LocaleStringResource localeStringResource);

        void AddOrUpdatePluginLocaleResource(string resourceName, string resourceValue);

        void DeletePluginLocaleResource(string resourceName);
        Dictionary<string, KeyValuePair<int, string>> GetAllResourceValues();

        string GetResource(string resourceKey);

        string GetResource(string resourceKey, 
            bool logIfNotFound = true, string defaultValue = "", bool returnEmptyIfNotFound = false);


    }
}
