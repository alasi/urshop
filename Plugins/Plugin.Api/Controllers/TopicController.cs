﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Urs.Core;
using Urs.Core.Caching;
using Urs.Data.Domain.Topics;
using Urs.Data.Domain.Configuration;
using Plugin.Api.Models.Topics;
using Urs.Services.Topics;
using Urs.Framework;

namespace Plugin.Api.Controllers
{
    /// <summary>
    /// 信息
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/topic")]
    [ApiController]
    public class TopicController : ControllerBase
    {
        private readonly ITopicService _topicService;
        private readonly AdminAreaSettings _adminAreaSettings;
        private readonly ICacheManager _cacheManager;

        /// <summary>
        /// 构造器
        /// </summary>
        public TopicController(ITopicService topicService,
            AdminAreaSettings adminAreaSettings,
            ICacheManager cacheManager)
        {
            this._topicService = topicService;
            this._adminAreaSettings = adminAreaSettings;
            this._cacheManager = cacheManager;
        }

        #region Utilities

        [NonAction]
        protected MoTopic PrepareTopicModel(string systemName)
        {
            var topic = _topicService.GetTopicBySystemName(systemName);
            if (topic == null)
                return null;

            var model = new MoTopic()
            {
                Id = topic.Id,
                SystemName = topic.SystemName,
                Published = topic.Published,
                Title = topic.Title,
                Body = topic.Body
            };
            return model;
        }

        protected void PrepareTopicModel(MoTopic model, Topic topic)
        {
            if (topic == null)
                throw new ArgumentNullException("topic");

            if (model == null)
                throw new ArgumentNullException("model");

            model.Id = topic.Id;
            model.SystemName = topic.SystemName;
            model.Title = topic.Title;
            model.Body = topic.Body;
            model.Short = topic.Short;
            model.Published = topic.Published;
        }

        #endregion

        /// <summary>
        /// 商城信息列表
        /// </summary>
        /// <param name="page">页码（默认：1）</param>
        /// <param name="size">页大小（默认值：12）</param>
        /// <returns></returns>
        /// <response code="200">成功请求,单页列表</response>
        [HttpGet("list")]
        [ProducesResponseType(typeof(MoTopicList), 200)]
        public async Task<ApiResponse<MoTopicList>> List(int page = 1, int size = 12)
        {
            var data = await Task.Run(() =>
            {
                var model = new MoTopicList();
                var command = new PagingFiltering
                {
                    PageNumber = page,
                    PageSize = size > 0 ? size : _adminAreaSettings.GridPageSize
                };

                var topics = _topicService.GetAllTopics(null, command.PageNumber - 1, command.PageSize);

                model.Paging.Page = page;
                model.Paging.Size = size;
                model.Paging.TotalPage = topics.TotalPages;
                model.Topics = topics
                    .Select(x =>
                    {
                        var topicModel = new MoTopic();
                        PrepareTopicModel(topicModel, x);
                        return topicModel;
                    })
                    .ToList();
                return model;
            });
            return ApiResponse<MoTopicList>.Success(data);
        }
        /// <summary>
        /// 商城信息
        /// </summary>
        /// <param name="systemName">信息系统名(mobileaboutus、contactus、mobilejoinus)</param>
        /// <returns></returns>
        [HttpGet("detail")]
        public async Task<ApiResponse<MoTopic>> TopicDetail(string systemName)
        {
            var data = await Task.Run(() =>
              {
                  return PrepareTopicModel(systemName);
              });

            if (data == null)
                return ApiResponse<MoTopic>.NotFound();

            return ApiResponse<MoTopic>.Success(data);
        }
    }
}
