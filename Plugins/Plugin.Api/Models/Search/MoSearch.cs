﻿using System.Collections.Generic;
using Plugin.Api.Models.Common;
using Plugin.Api.Models.Goods;

namespace Plugin.Api.Models.Search
{
    /// <summary>
    /// 搜索对象
    /// </summary>
    public partial class MoSearch
    {
        public MoSearch()
        {
            Items = new List<MoGoodsOverview>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// 查询关键字
        /// </summary>
        
        public string Q { get; set; }
        /// <summary>
        /// 商品分类Id
        /// </summary>
        public int Cid { get; set; }
        /// <summary>
        /// 是否搜索分类子类
        /// </summary>
        public bool Isc { get; set; }
        /// <summary>
        /// 品牌Id
        /// </summary>
        public int Mid { get; set; }
        /// <summary>
        /// 价格差-开始价格
        /// </summary>
        
        public string Pf { get; set; }
        /// <summary>
        /// 价格差-结束价格
        /// </summary>
        
        public string Pt { get; set; }
        /// <summary>
        /// 搜索描述
        /// </summary>
        public bool Sid { get; set; }
        /// <summary>
        /// 商品数组
        /// </summary>
        public IList<MoGoodsOverview> Items { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }
    }
}