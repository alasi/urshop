﻿using System.Collections.Generic;
using Urs.Data.Domain.Common;
using Plugin.Api.Models.ShoppingCart;

namespace Plugin.Api.Models.User
{
    /// <summary>
    /// 用户信息
    /// </summary>
    public partial class MoUserRequest
    {
        public MoUserRequest()
        {
            Form = new List<MoKeyValue>();
        }
        /// <summary>
        /// 用户昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public string Gender { get; set; }
        /// <summary>
        /// 头像Id
        /// </summary>
        public int AvatarPictureId { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        public string BirthDate { get; set; }
        /// <summary>
        /// 公司
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string StreetAddress { get; set; }
        /// <summary>
        /// 邮编
        /// </summary>
        public string Zip { get; set; }
        /// <summary>
        /// 省份Id
        /// </summary>
        public int ProvinceId { get; set; }
        /// <summary>
        /// 城市Id
        /// </summary>
        public int CityId { get; set; }
        /// <summary>
        /// 区域Id
        /// </summary>
        public int AreaId { get; set; }
        /// <summary>
        /// 传真
        /// </summary>
        public string Fax { get; set; }
        /// <summary>
        /// 提交自定义字段
        /// </summary>
        public IList<MoKeyValue> Form { get; set; }
    }
}