﻿using System;

namespace Plugin.Api.Models.Common
{
    public partial class MoConsult
    {
        /// <summary>
        /// 用户Guid [必填]
        /// </summary>
        public string UserGuid { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 咨询内容
        /// </summary>
        public virtual string Question { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}