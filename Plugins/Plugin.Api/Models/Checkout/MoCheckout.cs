﻿using System.Collections.Generic;
using Plugin.Api.Models.UserAddress;

namespace Plugin.Api.Models.Checkout
{
    /// <summary>
    /// 结算
    /// </summary>
    public class MoCheckout
    {
        public MoCheckout()
        {
            ShippingMethods = new MoShippingMethodList();
            Address = new MoAddress();
            Items = new List<MoCartItem>();
            Fields = new List<MoCustomField>();
        }
        /// <summary>
        /// 配送必填?
        /// </summary>
        public bool ShippingRequired { get; set; }
        /// <summary>
        /// 配送方法必填?
        /// </summary>
        public bool ShippingMethodRequired { get; set; }
        /// <summary>
        /// 配送地址
        /// </summary>
        public MoAddress Address { get; set; }
        /// <summary>
        /// 配送方法
        /// </summary>
        public MoShippingMethodList ShippingMethods { get; set; }
        /// <summary>
        /// 支付方法
        /// </summary>
        public string PaymentMethod { get; set; }
        /// <summary>
        /// 购物信息
        /// </summary>
        public IList<MoCartItem> Items { get; set; }
        /// <summary>
        /// 奖励积分
        /// </summary>
        public decimal RedeemedRewardPoints { get; set; }
        /// <summary>
        /// 积分等价金额
        /// </summary>
        public string RedeemedRewardPointsAmount { get; set; }
        /// <summary>
        /// 购物车小姐（商品小计）
        /// </summary>
        public string OrderSubtotal { get; set; }
        /// <summary>
        /// 配送金额
        /// </summary>
        public string Shopping { get; set; }
        /// <summary>
        /// 订单总额
        /// </summary>
        public string OrderTotal { get; set; }

        public IList<MoCustomField> Fields { get; set; }

        public partial class MoCustomField
        {
            public MoCustomField()
            {
                Values = new List<MoCustomValue>();
            }
            /// <summary>
            /// Id
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 名称
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 默认值
            /// </summary>
            public string DefaultValue { get; set; }
            /// <summary>
            /// 子项
            /// </summary>
            public IList<MoCustomValue> Values { get; set; }
        }
        public partial class MoCustomValue
        {
            /// <summary>
            /// 名称
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 预选
            /// </summary>
            public bool IsPreSelected { get; set; }
        }
    }
}