﻿using System.Collections.Generic;
using Urs.Framework.Mvc;
using Plugin.Api.Models.Media;

namespace Plugin.Api.Models.Catalog
{
    /// <summary>
    /// 商品分类列表
    /// </summary>
    public partial class MoCategoryList 
    {
        public MoCategoryList()
        {
            SubCategories = new List<MoCategoryList>();
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 分类url
        /// </summary>
        public string SeName { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 子分类
        /// </summary>
        public List<MoCategoryList> SubCategories { get; set; }
    }
}