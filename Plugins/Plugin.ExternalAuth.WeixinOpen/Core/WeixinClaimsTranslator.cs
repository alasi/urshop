
using System.Collections.Generic;
using Urs.Services.Authentication.External;
using Newtonsoft.Json.Linq;

namespace Urs.Plugin.ExternalAuth.WeixinOpen.Core
{
    public class WeixinOpenClaimsTranslator : IClaimsTranslator<JObject>
    {
        public UserClaims Translate(JObject response)
        {
            var claims = new UserClaims();

            if (response["nickname"] != null)
                claims.Nickname = response["nickname"].ToString();

            if (response["headimgurl"] != null)
                claims.HeadImgUrl = response["headimgurl"].ToString();
            
            if (response["gender"] != null)
                claims.Sex = response["sex"].ToString() == "1" ? "��" : "Ů";

            if (response["province"] != null || response["city"] != null)
            {
                claims.Province = response["province"].ToString();
                claims.City = response["city"].ToString();
            }

            return claims;
        }
    }
}