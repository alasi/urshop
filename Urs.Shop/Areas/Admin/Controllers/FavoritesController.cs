﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Urs.Admin.Models.Common;
using Urs.Services.Common;
using Urs.Services.Localization;
using Urs.Services.Logging;
using Urs.Services.Security;
using Urs.Framework.Kendoui;

namespace Urs.Admin.Controllers
{
    public partial class FavoritesController : BaseAdminController
    {
        #region Fields

        private readonly IFavoritesService _favoritesService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IActivityLogService _activityLogService;

        #endregion

        #region Ctor

        public FavoritesController(IFavoritesService favoritesService,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            IActivityLogService activityLogService)
        {
            this._favoritesService = favoritesService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._activityLogService = activityLogService;
        }

        #endregion

        #region Methods        

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFavorites))
                return HttpUnauthorized();

            var model = new FavoritesModel();
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult ListJson(PageRequest command, int userId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFavorites))
                return HttpUnauthorized();

            var list = _favoritesService.GetFavorites(userId, command.Page - 1, command.Limit);

            var result = new ResponseResult
            {
                data = list.Select(item =>
                {
                    var data = new FavoritesModel();
                    data.Id = item.Id;
                    data.CreateTime = item.CreateTime;
                    return data;
                }),
                count = list.TotalCount
            };
            return Json(result);
        }

        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFavorites))
                return HttpUnauthorized();

            var item = _favoritesService.GetById(id);
            if (item == null)
                return RedirectToAction("List");
            var model = new FavoritesModel();
            if (item != null)
            {
                model.Id = id;
                model.CreateTime = item.CreateTime;
            }
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Edit(FavoritesModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFavorites))
                return HttpUnauthorized();

            if (ModelState.IsValid)
            {
                var entity = _favoritesService.GetById(model.Id);

                _favoritesService.UpdateFavorites(entity);

                return Json(new { succes = 1 });
            }
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFavorites))
                return HttpUnauthorized();

            //try to get a blog post with the specified id
            var entity = _favoritesService.GetById(id);
            if (entity == null)
                return RedirectToAction("List");

            _favoritesService.DeleteFavorites(entity);

            //activity log
            _activityLogService.InsertActivity("DeleteFavorites",
                string.Format(_localizationService.GetResource("ActivityLog.DeleteFavorites"), entity.Id), entity);

            return RedirectToAction("List");
        }

        #endregion
    }
}