﻿using System;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Coupons
{
    public class CouponListModel : BaseEntityModel
    {
        public string UniqueCode { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        


    }
}