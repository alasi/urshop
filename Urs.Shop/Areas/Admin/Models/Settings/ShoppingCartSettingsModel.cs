﻿using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Settings
{
    public partial class ShoppingCartSettingsModel : BaseModel, ISettingsModel
    {
        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.DisplayCartAfterAddingGoods")]
        public bool DisplayCartAfterAddingGoods { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.DisplayWishlistAfterAddingGoods")]
        public bool DisplayWishlistAfterAddingGoods { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.MaximumShoppingCartItems")]
        public int MaximumShoppingCartItems { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.MaximumWishlistItems")]
        public int MaximumWishlistItems { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.AllowOutOfStockItemsToBeAddedToWishlist")]
        public bool AllowOutOfStockItemsToBeAddedToWishlist { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.MoveItemsFromWishlistToCart")]
        public bool MoveItemsFromWishlistToCart { get; set; }
        
        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.ShowGoodsImagesOnShoppingCart")]
        public bool ShowGoodsImagesOnShoppingCart { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.ShowGoodsImagesOnWishList")]
        public bool ShowGoodsImagesOnWishList { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.MinimunShoppingCartItemGoodsQuantity")]
        public int MinimunShoppingCartItemGoodsQuantity { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.CrossSellsNumber")]
        public int CrossSellsNumber { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.EmailWishlistEnabled")]
        public bool EmailWishlistEnabled { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.AllowAnonymousUsersToEmailWishlist")]
        public bool AllowAnonymousUsersToEmailWishlist { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.MiniShoppingCartEnabled")]
        public bool MiniShoppingCartEnabled { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.ShowGoodsImagesInMiniShoppingCart")]
        public bool ShowGoodsImagesInMiniShoppingCart { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.ShoppingCart.MiniShoppingCartGoodsNumber")]
        public int MiniShoppingCartGoodsNumber { get; set; }
    }
}