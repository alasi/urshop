﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Users
{
    public partial class BestUsersReportModel : BaseModel
    {
        public BestUsersReportModel()
        {
            AvailableOrderStatuses = new List<SelectListItem>();
            AvailablePaymentStatuses = new List<SelectListItem>();
            AvailableShippingStatuses = new List<SelectListItem>();
        }

        [UrsDisplayName("Admin.Users.Reports.BestBy.StartDate")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [UrsDisplayName("Admin.Users.Reports.BestBy.EndDate")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [UrsDisplayName("Admin.Users.Reports.BestBy.OrderStatus")]
        public int OrderStatusId { get; set; }
        [UrsDisplayName("Admin.Users.Reports.BestBy.PaymentStatus")]
        public int PaymentStatusId { get; set; }
        [UrsDisplayName("Admin.Users.Reports.BestBy.ShippingStatus")]
        public int ShippingStatusId { get; set; }

        public IList<SelectListItem> AvailableOrderStatuses { get; set; }
        public IList<SelectListItem> AvailablePaymentStatuses { get; set; }
        public IList<SelectListItem> AvailableShippingStatuses { get; set; }
    }
}