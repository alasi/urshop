﻿using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Users
{
    public partial class BestUserReportLineModel : BaseModel
    {
        public int UserId { get; set; }
        [UrsDisplayName("Admin.Users.Reports.BestBy.Fields.User")]
        public string UserName { get; set; }

        [UrsDisplayName("Admin.Users.Reports.BestBy.Fields.OrderTotal")]
        public string OrderTotal { get; set; }

        [UrsDisplayName("Admin.Users.Reports.BestBy.Fields.OrderCount")]
        public decimal OrderCount { get; set; }
    }
}