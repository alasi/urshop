﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using Urs.Admin.Validators.Stores;
using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    [Validator(typeof(GoodsParameterMappingValidator))]
    public partial class GoodsParameterModel : BaseEntityModel
    {
        public GoodsParameterModel()
        {
            Groups = new List<SelectListItem>();
        }
        [UrsDisplayName("Admin.Store.GoodsParameters.Fields.Name")]
        
        public string Name { get; set; }
        [UrsDisplayName("Admin.Store.GoodsParameters.Fields.ParameterGroupId")]
        public int ParameterGroupId { get; set; }
        public IList<SelectListItem> Groups { get; set; }

        [UrsDisplayName("Admin.Store.GoodsParameters.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

    }
}