﻿using Microsoft.AspNetCore.Mvc;
using UEditorNetCore;

namespace Urs.Web.Controllers
{
    //do not inherit it from BasePublicController. otherwise a lot of extra action filters will be called
    //they can create guest account(s), etc
    public partial class UEditorController : Controller
    {
        private UEditorService ue;
        public UEditorController(UEditorService ue)
        {
            this.ue = ue;
        }
        public void Do()
        {
            ue.DoAction(HttpContext);
        }
    }
}