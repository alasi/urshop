using Urs.Core;
using System.Collections.Generic;
using Urs.Data.Domain.Localization;

namespace Urs.Data.Domain.Stores
{
    /// <summary>
    /// ����ӳ��
    /// </summary>
    public partial class GoodsParameter : BaseEntity
    {
        private ICollection<GoodsParameterOption> _goodsParameterOptions;

        public virtual string Name { get; set; }

        public virtual int DisplayOrder { get; set; }

        public virtual bool Deleted { get; set; }

        public virtual int GoodsParameterGroupId { get; set; }

        public virtual ICollection<GoodsParameterOption> GoodsParameterOptions
        {
            get { return _goodsParameterOptions ?? (_goodsParameterOptions = new List<GoodsParameterOption>()); }
            protected set { _goodsParameterOptions = value; }
        }
    }
}
