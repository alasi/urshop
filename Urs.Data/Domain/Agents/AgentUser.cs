
using System;
using Urs.Core;

namespace Urs.Data.Domain.Agents
{
    public partial class AgentUser : BaseEntity
    {
        public virtual int UserId { get; set; }
        /// <summary>
        /// 分销商、团长、卖家 佣金
        /// </summary>
        public virtual decimal Rate { get; set; }
        /// <summary>
        /// 二级佣金
        /// </summary>
        public virtual decimal ParentRate { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
