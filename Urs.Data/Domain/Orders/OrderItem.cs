using Urs.Core;
using System;
using System.Collections.Generic;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Domain.Orders
{
    /// <summary>
    /// Represents an order item 
    /// </summary>
    public partial class OrderItem : BaseEntity
    {
        /// <summary>
        /// Gets or sets the order item identifier
        /// </summary>
        public virtual Guid OrderItemGuid { get; set; }

        /// <summary>
        /// 订单Id
        /// </summary>
        public virtual int OrderId { get; set; }

        /// <summary>
        /// 产品Id
        /// </summary>
        public virtual int GoodsId { get; set; }

        /// <summary>
        /// 产品名称
        /// </summary>
        public virtual string GoodsName { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public virtual int Quantity { get; set; }

        /// <summary>
        /// 单位价格
        /// </summary>
        public virtual decimal UnitPrice { get; set; }
        /// <summary>
        /// 组合产品sku
        /// </summary>
        public virtual string Sku { get; set; }

        /// <summary>
        /// 小计
        /// </summary>
        public virtual decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the attribute description
        /// </summary>
        public virtual string AttributeDescription { get; set; }

        /// <summary>
        /// Gets or sets the goods attributes in XML format
        /// </summary>
        public virtual string AttributesXml { get; set; }
        /// <summary>
        /// Gets or sets the total weight of one item
        /// </summary>
        public virtual decimal? ItemWeight { get; set; }
        /// <summary>
        /// Gets the order
        /// </summary>
        public virtual Order Order { get; set; }
        /// <summary>
        /// Gets the goods
        /// </summary>
        public virtual Goods Goods { get; set; }

    }
}
