
namespace Urs.Data.Domain.Payments
{
    /// <summary>
    /// 支付状态
    /// </summary>
    public enum PaymentStatus : int
    {
        /// <summary>
        /// 待支付
        /// </summary>
        Pending = 10,
        /// <summary>
        /// 授权支付（比如线下付款、赊账等）
        /// </summary>
        Authorized = 20,
        /// <summary>
        /// 已支付
        /// </summary>
        Paid = 30,
        /// <summary>
        /// 部分退款
        /// </summary>
        PartiallyRefunded = 35,
        /// <summary>
        /// 退款
        /// </summary>
        Refunded = 40,
        /// <summary>
        /// 无效
        /// </summary>
        Voided = 50,
        /// <summary>
        /// 红包
        /// </summary>
        LuckyMoney = 60,
        /// <summary>
        /// 佣金
        /// </summary>
        Rebate = 70
    }
}
