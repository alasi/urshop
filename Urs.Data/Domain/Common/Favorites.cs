﻿using Urs.Core;
using System;

namespace Urs.Data.Domain.Common
{
    public class Favorites : BaseEntity
    {
        public int GoodsId { get; set; }
        public int UserId { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
