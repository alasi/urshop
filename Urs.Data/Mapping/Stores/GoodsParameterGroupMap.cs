
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsParameterGroupMap : UrsEntityTypeConfiguration<GoodsParameterGroup>
    {
        public override void Configure(EntityTypeBuilder<GoodsParameterGroup> builder)
        {
            builder.ToTable(nameof(GoodsParameterGroup));
            builder.HasKey(sa => sa.Id);
            builder.Property(sa => sa.Name).IsRequired();
            base.Configure(builder);
        }
    }
}