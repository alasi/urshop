using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Directory;

namespace Urs.Data.Mapping.Directory
{
    public partial class AreaMap : UrsEntityTypeConfiguration<Area>
    {
        public override void Configure(EntityTypeBuilder<Area> builder)
        {
            builder.ToTable(nameof(Area));
            builder.HasKey(sp => sp.Id);
            builder.Property(sp => sp.AreaName).IsRequired().HasMaxLength(100);
            
            base.Configure(builder);
        }
    }
}