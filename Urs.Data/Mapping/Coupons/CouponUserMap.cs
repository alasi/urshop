﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Urs.Data.Domain.Coupons;

namespace Urs.Data.Mapping.Coupons
{
   public partial class CouponUserMap:UrsEntityTypeConfiguration<CouponUserMapping>
    {
        public override void Configure(EntityTypeBuilder<CouponUserMapping> builder)
        {
           builder.ToTable(nameof(CouponUserMapping));
           builder.HasKey(p => p.Id);

           builder.HasOne(p => p.Coupon)
               .WithMany()
               .HasForeignKey(p => p.CouponId);

           builder.HasOne(p => p.User)
               .WithMany()
               .HasForeignKey(p => p.UserId);

            base.Configure(builder);

        }
    }
}
