import api from '../../api/api'
var app = getApp()
import {
  base64src
} from '../../libs/base64src.js'
import {
  category
} from '../../api/conf'
import {
  searCate
} from '../../api/conf'
import {
  getUnlimited
} from '../../api/conf'
Page({
  data: {
    isModal: false, //是否显示拒绝保存图片后的弹窗
    select: 0,
    subcatelist: [],
    admin: 0,
    adminInfo: '',
    adminQr: '',
    catId: 0,
    subcateProduct: [],
    page: 1,
    subid: 0,
    isHome: true,
    qrCodeBase64: '',
    isCanDraw: false,
    stop: false
  },
  handleClose() {
    console.log(' handleClose()');
    this.setData({
      isCanDraw: false
    })

  },
  onLoad: function(options) {
    if (options.name) {
      wx.setNavigationBarTitle({
        title: options.name
      })
    }
    if (options.id) {
      this.setData({
        catId: options.id
      })
    }
    if (options.subid) {
      this.setData({
        select: options.subid,
        isHome: false
      })
    }
  },
  onReady: function() {

  },
  onShow: function() {
    this.getsubcate()
  },
  switchClick: function(e) {
    this.setData({
      select: e.currentTarget.dataset.id,
      catId: e.currentTarget.dataset.id,
      subcateProduct: [],
      page: 1
    })
    this.getSubCateProduct()
  },
  getsubcate: function() {
    var that = this
    api.get(category, {
      catId: that.data.catId
    }).then(res => {
      let id = that.data.catId
      if (res.Data.length) {
        id = res.Data[0].Id
      }

      that.setData({
        subcatelist: res.Data
      })
      if (that.data.isHome) {
        that.setData({
          select: id
        })
      }
      this.getSubCateProduct()
    })
  },
  getSubCateProduct: function() {
    var that = this
    var role = wx.getStorageSync('role')
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    api.get(searCate, {
      cid: that.data.select,
      page: that.data.page
    }).then(res => {
      if (!res.Data.Items.length) {
        wx.hideLoading();
        that.setData({
          stop: true
        })
        return;
      }
      var arr = [];
      var productlist = res.Data;
      for (let item of res.Data.Items) {
        arr.push(item['Id'])
      }
      that.setData({
        subcateProduct: that.data.subcateProduct.concat(productlist.Items)
      })
      wx.hideLoading()
    })
  },
  lower: function(e) {
    if (this.data.stop) {
      return;
    }
    this.setData({
      page: this.data.page + 1
    })
    this.getSubCateProduct()
  },
  handlePhotoSaved() {
    wx.showLoading({
      title: '正在保存...',
      mask: true
    })

    wx.saveImageToPhotosAlbum({

      filePath: this.data.qrCodeBase64,
      success: (res) => {
        wx.showToast({
          title: '保存成功',
          icon: 'none'
        })

      },
      fail: (res) => {
        wx.getSetting({
          success: res => {
            let authSetting = res.authSetting
            if (!authSetting['scope.writePhotosAlbum']) {
              this.setData({
                isModal: true
              })
              console.log(res);
            }
          }
        })
        setTimeout(() => {
          wx.hideLoading()
        }, 300)
      }
    })
  },
  copyBtn() {
    var that = this;
    wx.setClipboardData({
      //准备复制的数据
      data: that.data.adminInfo,
      success: function(res) {
        wx.showToast({
          title: '复制成功',
        });
      }
    });
  },
  createShareImage() {
    var that = this;
    if (that.data.qrCodeBase64) {
      that.setData({
        isCanDraw: !that.data.isCanDraw
      })
    } else {
      wx.showLoading({
        title: '获取小程序码'
      })
      wx.request({
        url: `${app.globalData.host}${getUnlimited}`,
        method: 'POST',
        data: {
          "scene": 'id=' + that.data.catId + '&code=' + wx.getStorageSync('code'),
          "page": "pages/subcategory/subcategory",
          "width": 1200,
          "auto_color": true,
          "line_color": {},
          "is_hyaline": false
        },
        success(res) {
          var base64 = 'data:image/jpeg;base64,' + res.data.Msg;

          base64src(base64, src => {
            wx.hideLoading()

            that.setData({
              qrCodeBase64: src,
              adminInfo: 'pages/subcategory/subcategory?id=' + that.data.catId,
              adminQr: base64,
              isCanDraw: !that.data.isCanDraw
            })


          });

        }
      })
    }





  }
})