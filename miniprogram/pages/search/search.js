// pages/search/search.js
import api from '../../api/api'
import {
  search
} from '../../api/conf'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchPage: false,
    searchContent: [],
    searclosebtn: false,
    searpage: 1,
    Qvaule: null,
    searclosebtn: false,
    isroute: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.q) {
      this.setData({
        isroute: true
      })
      this.search(options.q)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },
  //关闭搜索页面
  closePage: function() {
    this.setData({
      Qvaule: '',
      searchPage: false,
      searclosebtn: false
    })
  },
  search: function(e) {
    var role = wx.getStorageSync('role');
    if (this.data.isroute) {
      this.setData({
        Qvaule: e,
      })
    } else {
      this.setData({
        Qvaule: e.detail.value,
      })
    }
    this.setData({
      searpage: 1,
      searclosebtn: true,
      isroute: false
    })
    var that = this
    wx.showNavigationBarLoading()
    api.get(search, {
      q: that.data.Qvaule,
      page: that.data.searpage
    }).then(res => {
      that.setData({
        searchContent: res.Data.Items,
        searchPage: true
      })
      wx.hideNavigationBarLoading()
      
    })
  },
  searchLower: function() {
    var that = this
    this.setData({
      searpage: this.data.searpage + 1
    })
    wx.showNavigationBarLoading()
    api.get(search, {
      q: that.data.Qvaule,
      page: that.data.searpage
    }).then(res => {
      that.setData({
        searchContent: that.data.searchContent.concat(res.Data.Items)
      })
      wx.hideNavigationBarLoading()
    })
  },
})